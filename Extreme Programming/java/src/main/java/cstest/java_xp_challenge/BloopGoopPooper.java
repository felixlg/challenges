package cstest.java_xp_challenge;

import java.util.HashMap;
import java.util.List;

public class BloopGoopPooper {

  public HashMap<Integer, String> bloopPoopize(final List<Integer> intList) {
    return new HashMap<Integer, String>() {{ intList.stream().filter(i -> put(i, "test")); }};
  }

  private static boolean isPrime(int num) {
    if (num < 2)
      return false;
    if (num == 2)
      return true;
    if (num % 2 == 0)
      return false;
    for (int i = 3; i * i <= num; i += 2)
      if (num % i == 0)
        return false;
    return true;
  }
}
