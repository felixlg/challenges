# OS

## Super quiz!

### Instruction
Vous devez répondre au questionnaire directement dans ce README en dessous de chaque question. Ces questions touchent plusieurs aspects de la programmation de bas niveau et les réponses sont généralement assez courtes.

###### Question 1
Quelle est la différence entre le heap et le stack (le monceau et la pile).

Le stack est utilisé pour stocker les variables locales et les adresses de retour. Le heap c'est là où on alloue dynamiquement les ressources (par exemple avec le keyword new ou les tableaux dynamiques en C++)


###### Question 2
Que fais la fonction malloc?

Alloue de la mémoire sur le heap et retourne un pointeur vers le premier octet alloué.


###### Question 3
Décrivez le fonctionnement de ce code.

```
uint16_t lastKeys = 0;
uint16_t diff, currentKeys;

while (1) {
	currentKeys = scanKeyboard();
	if ((diff = (uint16_t) (currentKeys & (~lastKeys))) || !currentKeys)
		lastKeys = currentKeys;
	faire_quelque_chose(diff);
}
```

C'est une boucle infinie qui assigne currentKeys au retour de scanKeyboard(), assigne diff aux bits à 1 de currentKeys étant à 0 dans lastKeys. Puis il vérifie si diff n'est pas 0 si currentKeys est à 0, le cas échéant, assigne lastKeys à la valeur de currentKeys. Puis il appelle faire_quelque_chose() avec diff.

Essentiellement fait quelque chose avec les nouvelles clés appuyées à chaque tours de boucle.

###### Question 4
Que signifie les 8 paramètres de cette commande GCC:  
`gcc -Os -Wall -g3 -flto -lpthread -o main main.c`

Optimise pour la taille du binaire produit, montre tous les avertissements, ajouter au binaire des symboles de débugging (de niveau 3, soit avec l'expansion des macros), active le link-time-optimizer, et link la librarie pthread.

Il est bon de mentionner que la combinaison -g et -flto est expérimentale et attendue de retourner des résultats inattendus.


###### Question 5
À quoi sert le DMA dans un microcontrolleur ou un ordinateur.

Accéder à la ram sans passer par le cpu


###### Question 6
Quelle est la différence entre mutex et sémaphore?

un mutex verouille l'accès à une ressource. un sémaphore est un méchanisme de gestion d'accès à des ressources


###### Question 7
Un microcontrolleur lit, avec son convertisseur analogique à numérique à 12 bits, la température d'une cuve de fermentation à l'aide d'un thermomètre. Votre microcontrolleur fonctionne en 5V. Le thermomètre retourne une valeur entre 0 et 5V correspondant aux températures respectivement entre 15-25 degrées Celcius. Assumez que le comportement du thermomètre est linéaire.
Donnez la précision de lecture de votre microcontrolleur. Dans quel type de variable devriez-vous sauvegarder la valeur lue de façon à conserver le maximum de précision (char, int, long, float, double, etc), pourquoi? S'il y a lieu, expliquez aussi à quoi correspondent les valeurs enregistrées. (Max 10 lignes)


###### Question 8
Expliquer une façon de faire le "debounce" d'un bouton.


###### Question 9
Comment pourriez-vous faire pour mesurer la capacitance (en Farrad) d'un condensateur à l'aide d'un Arduino ?


###### Question 10
Que fait ce code et à quoi peut-il servir ? Existe-t-il un équivalent dans la librairie standard du C?

`while (*p++ = *q++) ;`

Copie le contenu de chaque élément du tableau q au même offset depuis p.
On pourrait implémenter strcpy() comme cela


###### Question 11
Décrivez les principales fonctions d'un microprocesseur.

processer :p
